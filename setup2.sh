#!/bin/bash


echo '

Your Shell: $SHELL

Downloading OH-MY-ZSH
running: sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
'
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"


echo "
Downloading and setup Poweline-Font: Hack-Fonts
"
wget https://github.com/powerline/fonts/raw/master/Hack/Hack-Regular.ttf
sudo mkdir /usr/share/fonts/Hack-Fonts/
sudo cp Hack-Regular.ttf /usr/share/fonts/Hack-Fonts/


echo 'Downloading ZSH plugins'
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting


echo '

--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

add this to ~/.zshrc file

Obviously before source $ZSH/oh-my-zsh.sh part

and Setup Terminal font as Hack


--------------------------------------------------

ZSH_THEME="agnoster"

plugins=(
    git
    history
    zsh-autosuggestions
    zsh-syntax-highlighting
    command-not-found
)

--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

'