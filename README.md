# zsh-setup-scripts


### 1st Run:

```bash
wget https://gitlab.com/ciqe/zsh-setup-scripts/-/raw/main/setup1.sh && sudo chmod +x setup1.sh && sudo ./setup1.sh
```

## Reboot your computer

```bash
wget https://gitlab.com/ciqe/zsh-setup-scripts/-/raw/main/setup2.sh && sudo chmod +x setup2.sh && sudo ./setup2.sh
```
